---
templateKey: blog-post
title: 'First official post! '
date: 2019-01-24T06:24:00.000Z
description: 'New blog is up, using Netlify + GatsbyJS!'
tags:
  - meta
---
So, just for reference, here's a screenshot of what the site looked like before making this post:

![new-blog-original-layout](/img/new-blog-original-layout.jpg "New blog, original layout")

Obviously, it's terrible. But it works, and is completely static, being hosted on Netlify's CDN. I'm going to use this project to learn GatsbyJS and Static Site Generators in general, which is something I haven't touched on much (front-end development isn't my strong suit).

There's a lot of ideas I have with regards to how to develop this, but the main thing is having somewhere I can publish the things I want to publish, without worrying about a back-end or version upgrades, maintenance, etc. This is the main reason I am moving away from Wordpress, which was always my go-to for simple sites like this.

There's still a long way for me to go with this, including getting a better Content Management System that allows easy posting with embedded images. If you're reading this and you have any recommendations of something to use (I currently have NetlifyCMS), please submit it to me using the Ask a Question link at the top.

Within the next few days I want to get some of my commonly asked questions up, and start publishing things more regularly, probably a couple of times a week. Welcome to 2019!
