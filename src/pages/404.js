import React from 'react'
import Layout from '../components/Layout'

const NotFoundPage = () => (
  <Layout>
        <section className="section section--gradient">
      <div className="container">
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="section">
              <h2 className="title is-size-3 has-text-weight-bold is-bold-light">
                404 PAGE NOT FOUND
              </h2>
              <p>The URL you have tried to visit does not exist. Please select a page from the menu options above.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </Layout>
)

export default NotFoundPage
